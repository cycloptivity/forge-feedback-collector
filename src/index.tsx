import ForgeUI, {
  Button,
  Form,
  Fragment,
  Text,
  TextArea,
  TextField,
  render,
  useProductContext,
  useState
} from "@forge/ui";
import api from "@forge/api";

import defaultConfig from "./config";
import { createIssueRequestBody } from "./issue";

type FormData = {
  summary: string;
  description: string;
};

enum State {
  Success,
  Error,
  NotSubmitted
}

const App = () => {
  const { accountId } = useProductContext();
  const [state, setState] = useState<State>(State.NotSubmitted);

  const handleSubmit = async (formData: FormData) => {
    const { summary, description } = formData;

    const response = await api.asApp().requestJira("/rest/api/3/issue", {
      headers: {
        "Content-Type": "application/json"
      },
      method: "POST",
      body: JSON.stringify(
        createIssueRequestBody({
          projectKey: process.env.PROJECT_KEY || defaultConfig.PROJECT_KEY,
          reporterAccountId: accountId,
          issueTypeName:
            process.env.ISSUE_TYPE_NAME || defaultConfig.ISSUE_TYPE_NAME,
          summary,
          description
        })
      )
    });

    setState(response.ok ? State.Success : State.Error);
  };

  if (state === State.NotSubmitted) {
    return (
      <Form onSubmit={handleSubmit}>
        <Text content="Submit feedback" />
        <TextField label="Summary" name="summary" />
        <TextArea label="Submit feedback" name="description" />
      </Form>
    );
  }

  return (
    <Fragment>
      <Text
        content={
          state === State.Success
            ? "Feedback submitted successfully!"
            : "Something went wrong."
        }
      />
      <Button
        text={state === State.Success ? "Create more feedback" : "Try again"}
        onClick={() => setState(State.NotSubmitted)}
      />
    </Fragment>
  );
};

export const run = render(<App />);
